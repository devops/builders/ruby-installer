SHELL = /bin/bash

base_image = gitlab-registry.oit.duke.edu/devops/containers/app-base:main

ruby_major ?= 2.6

ifeq ($(ruby_major), 2.6)
        ruby_version = 2.6.6
        ruby_download_sha256 = 5db187882b7ac34016cd48d7032e197f07e4968f406b0690e20193b9b424841f
else ifeq ($(ruby_major), 2.7)
        ruby_version = 2.7.2
        ruby_download_sha256 = 1b95ab193cc8f5b5e59d2686cb3d5dcf1ddf2a86cb6950e0b4bdaae5040ec0d6
else ifeq ($(ruby_major), 3.0)
	ruby_version = 3.0.0
	ruby_download_sha256 = 68bfaeef027b6ccd0032504a68ae69721a70e97d921ff328c0c8836c798f6cb1
endif

build_tag ?= ruby-$(ruby_version)

.PHONY : build
build:
	docker pull $(base_image)
	DOCKER_BUILDKIT=1 docker build -t $(build_tag) \
		--build-arg base_image=$(base_image) \
		--build-arg ruby_major=$(ruby_major) \
		--build-arg ruby_version=$(ruby_version) \
		--build-arg ruby_download_sha256=$(ruby_download_sha256) \
		./src

.PHONY : clean
clean:
	echo 'no-op'

.PHONY : test
test:
	docker run --rm $(build_tag) ruby -e 'puts "Hello, world!"'
