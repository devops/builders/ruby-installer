# Ruby installer

Based on our standard Debian buster base image, installs Ruby from source.

Supports Ruby 2.6 and 2.7.  Minor versions may vary.

This image is application agnostic, and should be appropriate for general
Ruby usage.
